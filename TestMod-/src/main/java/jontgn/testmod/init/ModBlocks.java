package jontgn.testmod.init;

import jontgn.testmod.blocks.BlockBase;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

import java.util.ArrayList;
import java.util.List;

public class ModBlocks {
    public static final List<Block> BLOCKS = new ArrayList<Block>();

    public static final Block GENERATOR_IRON = new BlockBase("generator_iron", Material.IRON);


}
