package jontgn.testmod.init;

import jontgn.testmod.items.ItemBase;
import net.minecraft.item.Item;

import java.util.ArrayList;
import java.util.List;

public class ModItems {
    public static final List<Item> ITEMS = new ArrayList<Item>();

    public static final Item NULL_ITEM = new ItemBase("null_item");
}
