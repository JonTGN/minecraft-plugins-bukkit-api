package me.jontgn.explodeall;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

public final class ExplodeAll extends JavaPlugin implements Listener {
    Block eventLocation;
    double xBlockLocation;
    double yBlockLocation;
    double zBlockLocation;
    String blockID;

    @Override
    public void onEnable() {
        // Plugin startup logic
        // System.out.println("Started- ExplodeAll.java");
        getServer().getPluginManager().registerEvents(this, this);
        blockID = null;
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        // player.sendMessage(player + " broke a block!!!");
        eventLocation = (Block) e.getBlock();
        xBlockLocation = eventLocation.getX();
        yBlockLocation = eventLocation.getY();
        zBlockLocation = eventLocation.getZ();

        float power = 4F;

        Material blockMaterialID = e.getBlock().getType();
        // System.out.println("block id is " + blockMaterialID);

        blockID = new String(String.valueOf(blockMaterialID));

        // System.out.println("block id string is: " + blockID);
        if (blockID.equals("COAL_ORE") || blockID.equals("IRON_ORE") || blockID.equals("NETHER_QUARTZ_ORE")) {
            power = 6F;
        } else if (blockID.equals("LAPIS_ORE") || blockID.equals("REDSTONE_ORE") || blockID.equals("GOLD_ORE") || blockID.equals("OBSIDIAN")) {
            power = 12f;
        } else if (blockID.equals("DIAMOND_ORE") || blockID.equals("EMERALD_ORE")) {
            power = 20f;
        }

        Bukkit.getServer().getWorld("world_nether").createExplosion(xBlockLocation, yBlockLocation, zBlockLocation, power, false, true);
        Bukkit.getServer().getWorld("world_the_end").createExplosion(xBlockLocation, yBlockLocation, zBlockLocation, power, false, true);
        Bukkit.getServer().getWorld("world").createExplosion(xBlockLocation, yBlockLocation, zBlockLocation, power, false, true);
        // change explosion damage?
        // create event to prevent damage to all entities, handler for EntityDamageEvent and check for cause of
        // ENTITY_EXPLOSION or BLOCK_EXPLOSION

        // if the entity is a player under t1 blocks, deal x damage
        // if the entity is a player under t2 blocks, deal 2x damage

        // Make rare ores immune to explosion damage from other blocks besides themselves


        // System.out.println("block break happened at" + eventLocation);
    }

    @EventHandler
    public void onBlockExplode(BlockExplodeEvent e) {
        e.setYield(50F);
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        EntityDamageEvent.DamageCause cause = e.getCause();
        Entity entity = e.getEntity();
        if (entity instanceof Player) {
            if (cause == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) {

                switch (blockID) {
                    case "COAL_ORE":
                        e.setDamage(2.0);
                        break;
                    case "IRON_ORE":
                    case "NETHER_QUARTZ_ORE":
                        e.setDamage(4.0);
                        break;
                    case "LAPIS_ORE":
                    case "REDSTONE_ORE":
                        e.setDamage(8.0);
                        break;
                    case "GOLD_ORE":
                    case "OBSIDIAN":
                        e.setDamage(12.0);
                        break;
                    case "DIAMOND_ORE":
                    case "EMERALD_ORE":
                        e.setDamage(19.0);
                        break;
                    default:
                        e.setDamage(1.0);
                        break;
                }

                System.out.println("BlockID is: " + blockID);
                // System.out.println("Damage location was at: " + loc);
            }
        }

        // Damage cause: BLOCK_EXPLOSION
        // System.out.println("Cause of damage was: " + cause);
        // System.out.println(entity + " was damaged by this event.");
    }
}
