package jontgn.testmod.util;

public class Reference {

    public static final String MOD_ID = "tm";
    public static final String NAME = "Test Mod JonTGN";
    public static final String VERSION = "1.0";
    public static final String ACCEPTED_VERSIONS = "1.12.2";
    public static final String CLIENT_PROXY_CLASS = "jontgn.testmod.proxy.ClientProxy";
    public static final String COMMON_PROXY_CLASS = "jontgn.testmod.proxy.CommonProxy";

}
